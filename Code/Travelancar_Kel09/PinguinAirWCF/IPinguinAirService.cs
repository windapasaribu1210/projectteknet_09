﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PinguinAirWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPinguinAirService" in both code and config file together.
    [ServiceContract]
    public interface IPinguinAirService
    {
        [OperationContract]
        List<Penerbangan3> getAllDetails();

        [OperationContract]
        List<Penerbangan3> getID(int id);

        [OperationContract]
        List<Penerbangan3> getAsal(string asal);

    }
}
