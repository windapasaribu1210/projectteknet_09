﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PinguinAirWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PinguinAirService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PinguinAirService.svc or PinguinAirService.svc.cs at the Solution Explorer and start debugging.
    public class PinguinAirService : IPinguinAirService
    {
        PinguinAirEntities objDBpinguin = new PinguinAirEntities();
        public List<Penerbangan3> getAllDetails()
        {
            return objDBpinguin.Penerbangan3.ToList();
        }

        public List<Penerbangan3> getAsal(string asal)
        {
            return objDBpinguin.Penerbangan3.Where(p => p.Asal == asal).ToList();
        }

        public List<Penerbangan3> getID(int id)
        {
            return objDBpinguin.Penerbangan3.Where(p => p.Id == id).ToList();
        }
    }
}
