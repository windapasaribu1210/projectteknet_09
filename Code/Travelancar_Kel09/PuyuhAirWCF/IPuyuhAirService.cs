﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PuyuhAirWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPuyuhAirService" in both code and config file together.
    [ServiceContract]
    public interface IPuyuhAirService
    {
        [OperationContract]
        List<Penerbangan> getAllDetails();

        [OperationContract]
        List<Penerbangan> getID(int id);

        [OperationContract]
        List<Penerbangan> getAsal(string asal);

    }
}
