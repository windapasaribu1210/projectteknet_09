﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.UI.WebControls;

namespace PuyuhAirWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PuyuhAirService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PuyuhAirService.svc or PuyuhAirService.svc.cs at the Solution Explorer and start debugging.
    public class PuyuhAirService : IPuyuhAirService
    {
        PuyuhAirEntities objDBpuyuh = new PuyuhAirEntities();
        public List<Penerbangan> getAllDetails()
        {
            return objDBpuyuh.Penerbangans.ToList();
        }

        public List<Penerbangan> getAsal(string asal)
        {
            return objDBpuyuh.Penerbangans.Where(p => p.Asal== asal).ToList();
        }

        public List<Penerbangan> getID(int id)
        {
            return objDBpuyuh.Penerbangans.Where(p => p.Id == id).ToList();
        }
    }
}


