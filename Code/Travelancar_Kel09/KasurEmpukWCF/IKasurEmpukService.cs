﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace KasurEmpukWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IKasurEmpukService" in both code and config file together.
    [ServiceContract]
    public interface IKasurEmpukService
    {
        [OperationContract]
        List<Hotel> getAllDetails();

        [OperationContract]
        List<Hotel> getID(int id);

        [OperationContract]
        List<Hotel> getKotal(string kota);
    }
}
