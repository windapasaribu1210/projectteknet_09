﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace KasurEmpukWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "KasurEmpukService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select KasurEmpukService.svc or KasurEmpukService.svc.cs at the Solution Explorer and start debugging.
    public class KasurEmpukService : IKasurEmpukService
    {
        KasurEmpukEntities objDBempuk = new KasurEmpukEntities();
        public List<Hotel> getAllDetails()
        {
            return objDBempuk.Hotels.ToList();
        }

        public List<Hotel> getID(int id)
        {
            return objDBempuk.Hotels.Where(p => p.Id == id).ToList();
        }

        public List<Hotel> getKotal(string kota)
        {
            return objDBempuk.Hotels.Where(p => p.Nama_Kota == kota).ToList();
        }
    }
}
