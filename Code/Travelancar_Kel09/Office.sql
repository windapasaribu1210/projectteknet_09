USE [Office]
GO
/****** Object:  Table [dbo].[Hotel]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Kota] [nvarchar](50) NULL,
	[Room] [nvarchar](50) NULL,
	[Harga] [money] NULL,
 CONSTRAINT [PK_Hotel] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Hotel2]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel2](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nama_Kota] [nvarchar](50) NULL,
	[Room] [nvarchar](50) NULL,
	[Harga] [money] NULL,
 CONSTRAINT [PK_Hotel2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Penerbangan]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Penerbangan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Asal] [nvarchar](50) NULL,
	[Tujuan] [nvarchar](50) NULL,
	[Waktu_Keberangkatan] [time](7) NULL,
	[Waktu_Sampai] [time](7) NULL,
	[Tanggal_Keberangkatan] [date] NULL,
	[Harga] [money] NULL,
 CONSTRAINT [PK_Penerbangan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Penerbangan2]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Penerbangan2](
	[Id] [int] NOT NULL,
	[Asal] [nvarchar](50) NULL,
	[Tujuan] [nvarchar](50) NULL,
	[Waktu_Keberangkatan] [time](7) NULL,
	[Waktu_Sampai] [time](7) NULL,
	[Tanggal_Keberangkatan] [date] NULL,
	[Harga] [money] NULL,
 CONSTRAINT [PK_Penerbangan2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Penerbangan3]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Penerbangan3](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Asal] [nvarchar](50) NULL,
	[Tujuan] [nvarchar](50) NULL,
	[Waktu_Keberangkatan] [time](7) NULL,
	[Waktu_Sampai] [time](7) NULL,
	[Tanggal_Keberangkatan] [date] NULL,
	[Harga] [money] NULL,
 CONSTRAINT [PK_Penerbangan3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Penumpangs]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Penumpangs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [nvarchar](max) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[NoHp] [nvarchar](max) NOT NULL,
	[Asal] [nvarchar](max) NULL,
	[Tujuan] [nvarchar](max) NOT NULL,
	[Tgl_Berangkat] [nvarchar](max) NOT NULL,
	[Kelas] [nvarchar](max) NOT NULL,
	[Maskapai] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Penumpangs] PRIMARY KEY CLUSTERED 
( 
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
 
GO
/****** Object:  Table [dbo].[PesananHotel]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PesananHotel](
	[No_Kamar] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [nvarchar](50) NULL,
	[Alamat] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[No_Telepon] [nvarchar](50) NULL,
	[CheckIn] [time](7) NULL,
	[CheckOut] [time](7) NULL,
	[Tanggal_Booking] [date] NULL,
 CONSTRAINT [PK_PesananHotel] PRIMARY KEY CLUSTERED 
(
	[No_Kamar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PesanPesawat]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PesanPesawat](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nama] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[No_Hp] [nvarchar](50) NULL,
	[Asal] [nvarchar](50) NULL,
	[Tujuan] [nvarchar](50) NULL,
	[Tanggal_Berangkat] [nvarchar](50) NULL,
	[Kelas] [nvarchar](50) NULL,
 CONSTRAINT [PK_PesanPesawat] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 6/7/2020 8:58:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Hotel] ON 

INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (1, N'Balige', N'Single Room', 300.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (2, N'Balige', N'Double Room', 500.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (3, N'Balige', N'Family Room', 1000.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (4, N'Laguboti', N'Single Room', 400.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (5, N'Laguboti', N'Double Room', 600.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (6, N'Laguboti', N'Family Room', 850.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (7, N'Tarutung', N'Single Room', 400.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (8, N'Tarutung', N'Double Room', 600.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (9, N'Tarutung', N'Family Room', 1000.0000)
INSERT [dbo].[Hotel] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (10, N'Porsea', N'Single Room', 250.0000)
SET IDENTITY_INSERT [dbo].[Hotel] OFF
SET IDENTITY_INSERT [dbo].[Hotel2] ON 

INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (1, N'Balige', N'Single Room', 350.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (2, N'Balige', N'Double Room', 550.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (3, N'Balige', N'Family Room', 850.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (4, N'Laguboti', N'Single Room', 300.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (5, N'Laguboti', N'Double Room', 600.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (6, N'Laguboti', N'Family Room', 850.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (7, N'Tarutung', N'Single Room', 450.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (8, N'Tarutung', N'Double Room', 550.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (9, N'Tarutung', N'Family Room', 1000.0000)
INSERT [dbo].[Hotel2] ([Id], [Nama_Kota], [Room], [Harga]) VALUES (10, N'Porsea', N'Single Room', 300.0000)
SET IDENTITY_INSERT [dbo].[Hotel2] OFF
SET IDENTITY_INSERT [dbo].[Penerbangan] ON 

INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (1, N'Jakarta', N'Solo', CAST(N'08:00:00' AS Time), CAST(N'10:00:00' AS Time), CAST(N'2020-04-11' AS Date), 500.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (2, N'Jakarta', N'Pekanbaru', CAST(N'10:15:00' AS Time), CAST(N'13:30:00' AS Time), CAST(N'2020-04-12' AS Date), 750.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (3, N'Jakarta', N'Surabaya', CAST(N'13:00:00' AS Time), CAST(N'15:00:00' AS Time), CAST(N'2020-04-13' AS Date), 1500.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (4, N'Silangit', N'Jakarta', CAST(N'10:00:00' AS Time), CAST(N'12:15:00' AS Time), CAST(N'2020-04-14' AS Date), 1000.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (5, N'Silangit', N'Medan', CAST(N'11:00:00' AS Time), CAST(N'12:30:00' AS Time), CAST(N'2020-04-15' AS Date), 300.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (6, N'Jakarta', N'Medan', CAST(N'13:00:00' AS Time), CAST(N'16:00:00' AS Time), CAST(N'2020-04-16' AS Date), 1000.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (7, N'Jakarta', N'Medan', CAST(N'18:00:00' AS Time), CAST(N'20:30:00' AS Time), CAST(N'2020-04-11' AS Date), 800.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (8, N'Medan', N'Jakarta', CAST(N'21:00:00' AS Time), CAST(N'23:00:00' AS Time), CAST(N'2020-04-17' AS Date), 600.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (9, N'Silangit', N'Solo', CAST(N'17:00:00' AS Time), CAST(N'20:00:00' AS Time), CAST(N'2020-04-18' AS Date), 700.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (10, N'Pekanbaru', N'Silangit', CAST(N'13:00:00' AS Time), CAST(N'15:00:00' AS Time), CAST(N'2020-04-19' AS Date), 1000.0000)
INSERT [dbo].[Penerbangan] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (11, N'Medan', N'Silangit', CAST(N'08:00:00' AS Time), CAST(N'10:00:00' AS Time), CAST(N'2020-05-30' AS Date), 500.0000)
SET IDENTITY_INSERT [dbo].[Penerbangan] OFF
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (1, N'Jakarta', N'Solo', CAST(N'08:00:00' AS Time), CAST(N'10:00:00' AS Time), CAST(N'2020-04-11' AS Date), 520.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (2, N'Jakarta', N'Pekanbaru', CAST(N'10:15:00' AS Time), CAST(N'13:30:00' AS Time), CAST(N'2020-04-12' AS Date), 700.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (3, N'Jakarta', N'Surabaya', CAST(N'13:00:00' AS Time), CAST(N'15:00:00' AS Time), CAST(N'2020-04-13' AS Date), 1500.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (4, N'Silangit', N'Jakarta', CAST(N'10:00:00' AS Time), CAST(N'12:15:00' AS Time), CAST(N'2020-04-14' AS Date), 1000.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (5, N'Silangit', N'Medan', CAST(N'11:00:00' AS Time), CAST(N'12:30:00' AS Time), CAST(N'2020-04-15' AS Date), 350.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (6, N'Medan', N'Silangit', CAST(N'13:00:00' AS Time), CAST(N'16:00:00' AS Time), CAST(N'2020-04-16' AS Date), 1000.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (7, N'Pekanbaru', N'Jakarta', CAST(N'18:00:00' AS Time), CAST(N'20:30:00' AS Time), CAST(N'2020-04-17' AS Date), 800.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (8, N'Solo', N'Surabaya', CAST(N'21:00:00' AS Time), CAST(N'23:00:00' AS Time), CAST(N'2020-04-18' AS Date), 850.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (9, N'Silangit', N'Medan', CAST(N'17:00:00' AS Time), CAST(N'20:00:00' AS Time), CAST(N'2020-04-19' AS Date), 650.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (10, N'Medan', N'Jakarta', CAST(N'13:00:00' AS Time), CAST(N'15:00:00' AS Time), CAST(N'2020-04-20' AS Date), 700.0000)
INSERT [dbo].[Penerbangan2] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (11, N'Surabaya', N'Solo', CAST(N'10:00:00' AS Time), CAST(N'12:00:00' AS Time), CAST(N'2020-04-21' AS Date), 1000.0000)
SET IDENTITY_INSERT [dbo].[Penerbangan3] ON 

INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (1, N'Jakarta', N'Solo', CAST(N'08:00:00' AS Time), CAST(N'10:00:00' AS Time), CAST(N'2020-04-12' AS Date), 500.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (2, N'Jakarta', N'Pekanbaru', CAST(N'10:00:00' AS Time), CAST(N'13:30:00' AS Time), CAST(N'2020-04-13' AS Date), 700.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (3, N'Jakarta', N'Surabaya', CAST(N'13:00:00' AS Time), CAST(N'15:00:00' AS Time), CAST(N'2020-04-14' AS Date), 800.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (4, N'Silangit', N'Jakarta', CAST(N'10:00:00' AS Time), CAST(N'12:15:00' AS Time), CAST(N'2020-04-15' AS Date), 300.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (5, N'Silangit', N'Medan', CAST(N'11:00:00' AS Time), CAST(N'12:30:00' AS Time), CAST(N'2020-04-16' AS Date), 450.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (6, N'Jakarta', N'Medan', CAST(N'13:00:00' AS Time), CAST(N'16:00:00' AS Time), CAST(N'2020-04-17' AS Date), 1000.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (7, N'Jakarta', N'Medan', CAST(N'18:00:00' AS Time), CAST(N'20:30:00' AS Time), CAST(N'2020-04-18' AS Date), 1200.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (8, N'Medan', N'Solo', CAST(N'21:00:00' AS Time), CAST(N'13:00:00' AS Time), CAST(N'2020-04-19' AS Date), 1500.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (9, N'Silangit', N'Jakarta', CAST(N'17:00:00' AS Time), CAST(N'20:00:00' AS Time), CAST(N'2020-04-20' AS Date), 750.0000)
INSERT [dbo].[Penerbangan3] ([Id], [Asal], [Tujuan], [Waktu_Keberangkatan], [Waktu_Sampai], [Tanggal_Keberangkatan], [Harga]) VALUES (10, N'Pekanbaru', N'Silangit', CAST(N'13:00:00' AS Time), CAST(N'15:00:00' AS Time), CAST(N'2020-04-21' AS Date), 400.0000)
SET IDENTITY_INSERT [dbo].[Penerbangan3] OFF
SET IDENTITY_INSERT [dbo].[Penumpangs] ON 

INSERT [dbo].[Penumpangs] ([id], [Nama], [Email], [NoHp], [Asal], [Tujuan], [Tgl_Berangkat], [Kelas], [Maskapai]) VALUES (1, N'dfgh', N'sdfghj', N'sdfgh', N'sdfghj', N'asdfghjk', N'sdfghjk', N'dfghj', N'dfgh')
INSERT [dbo].[Penumpangs] ([id], [Nama], [Email], [NoHp], [Asal], [Tujuan], [Tgl_Berangkat], [Kelas], [Maskapai]) VALUES (2, N'sdfghj', N'sdfghjk', N'sdfghj', N'sdfghj', N'dfghjk', N'asdfghj', N'asdfghj', N'sdfghjk')
SET IDENTITY_INSERT [dbo].[Penumpangs] OFF
SET IDENTITY_INSERT [dbo].[PesanPesawat] ON 

INSERT [dbo].[PesanPesawat] ([Id], [Nama], [Email], [No_Hp], [Asal], [Tujuan], [Tanggal_Berangkat], [Kelas]) VALUES (1, N'Rafika', N'rafikatampubolon18@gmail.com', N'082274883856', N'Balige', N'Jakarta', N'20-10-2020', N'Ekonomi')
INSERT [dbo].[PesanPesawat] ([Id], [Nama], [Email], [No_Hp], [Asal], [Tujuan], [Tanggal_Berangkat], [Kelas]) VALUES (2, N'Rafika', N'rafikatampubolon18@gmail.com', N'082274883856', N'Balige', N'Jakarta', N'20-10-2020', N'Ekonomi')
INSERT [dbo].[PesanPesawat] ([Id], [Nama], [Email], [No_Hp], [Asal], [Tujuan], [Tanggal_Berangkat], [Kelas]) VALUES (3, N'Rafika', N'rafikatampubolon18@gmail.com', N'082274883856', N'Balige', N'Jakarta', N'20-10-2020', N'Ekonomi')
INSERT [dbo].[PesanPesawat] ([Id], [Nama], [Email], [No_Hp], [Asal], [Tujuan], [Tanggal_Berangkat], [Kelas]) VALUES (4, N'Rafika', N'rafikatampubolon18@gmail.com', N'082274883856', N'Balige', N'Jakarta', N'20-10-2020', N'Ekonomi')
SET IDENTITY_INSERT [dbo].[PesanPesawat] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [UserName], [Password]) VALUES (1, N'admin', N'admin')
INSERT [dbo].[User] ([Id], [UserName], [Password]) VALUES (2, N'Rafika', N'rafika')
INSERT [dbo].[User] ([Id], [UserName], [Password]) VALUES (3, N'Nevi', N'nevi')
SET IDENTITY_INSERT [dbo].[User] OFF
