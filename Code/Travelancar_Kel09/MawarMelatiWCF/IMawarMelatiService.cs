﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MawarMelatiWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMawarMelatiService" in both code and config file together.
    [ServiceContract]
    public interface IMawarMelatiService
    {
        [OperationContract]
        List<Hotel2> getAllDetails();

        [OperationContract]
        List<Hotel2> getID(int id);

        [OperationContract]
        List<Hotel2> getKotal(string kota);
    }
}
