﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace MawarMelatiWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MawarMelatiService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MawarMelatiService.svc or MawarMelatiService.svc.cs at the Solution Explorer and start debugging.
    public class MawarMelatiService : IMawarMelatiService
    {
        MawarMelatiEntities objDBmawar = new MawarMelatiEntities();
        public List<Hotel2> getAllDetails()
        {
            return objDBmawar.Hotel2.ToList();
        }

        public List<Hotel2> getID(int id)
        {
            return objDBmawar.Hotel2.Where(p => p.Id == id).ToList();
        }

        public List<Hotel2> getKotal(string kota)
        { 
            return objDBmawar.Hotel2.Where(p => p.Nama_Kota == kota).ToList();
        }
    }
}
