﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ItikAirWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ItikAirService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ItikAirService.svc or ItikAirService.svc.cs at the Solution Explorer and start debugging.
    public class ItikAirService : IItikAirService
    {
        ItikAirEntities objDBitik = new ItikAirEntities();
        public List<Penerbangan2> getAllDetails()
        {
            return objDBitik.Penerbangan2.ToList();
        }

        public List<Penerbangan2> getAsal(string asal)
        {
            return objDBitik.Penerbangan2.Where(p => p.Asal == asal).ToList();

        }

        public List<Penerbangan2> getID(int id)
        {
            return objDBitik.Penerbangan2.Where(p => p.Id == id).ToList();

        }
    }
}
