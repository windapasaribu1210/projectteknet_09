﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ItikAirWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IItikAirService" in both code and config file together.
    [ServiceContract]
    public interface IItikAirService
    {
        [OperationContract]
        List<Penerbangan2> getAllDetails();

        [OperationContract]
        List<Penerbangan2> getID(int id);

        [OperationContract]
        List<Penerbangan2> getAsal(string asal);
    }
}
