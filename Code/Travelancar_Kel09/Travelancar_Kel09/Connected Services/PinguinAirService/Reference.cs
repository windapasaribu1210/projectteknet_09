﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Travelancar_Kel09.PinguinAirService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Penerbangan3", Namespace="http://schemas.datacontract.org/2004/07/PinguinAirWCF")]
    [System.SerializableAttribute()]
    public partial class Penerbangan3 : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string AsalField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<decimal> HargaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.DateTime> Tanggal_KeberangkatanField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TujuanField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.TimeSpan> Waktu_KeberangkatanField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<System.TimeSpan> Waktu_SampaiField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Asal {
            get {
                return this.AsalField;
            }
            set {
                if ((object.ReferenceEquals(this.AsalField, value) != true)) {
                    this.AsalField = value;
                    this.RaisePropertyChanged("Asal");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<decimal> Harga {
            get {
                return this.HargaField;
            }
            set {
                if ((this.HargaField.Equals(value) != true)) {
                    this.HargaField = value;
                    this.RaisePropertyChanged("Harga");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id {
            get {
                return this.IdField;
            }
            set {
                if ((this.IdField.Equals(value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.DateTime> Tanggal_Keberangkatan {
            get {
                return this.Tanggal_KeberangkatanField;
            }
            set {
                if ((this.Tanggal_KeberangkatanField.Equals(value) != true)) {
                    this.Tanggal_KeberangkatanField = value;
                    this.RaisePropertyChanged("Tanggal_Keberangkatan");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Tujuan {
            get {
                return this.TujuanField;
            }
            set {
                if ((object.ReferenceEquals(this.TujuanField, value) != true)) {
                    this.TujuanField = value;
                    this.RaisePropertyChanged("Tujuan");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.TimeSpan> Waktu_Keberangkatan {
            get {
                return this.Waktu_KeberangkatanField;
            }
            set {
                if ((this.Waktu_KeberangkatanField.Equals(value) != true)) {
                    this.Waktu_KeberangkatanField = value;
                    this.RaisePropertyChanged("Waktu_Keberangkatan");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<System.TimeSpan> Waktu_Sampai {
            get {
                return this.Waktu_SampaiField;
            }
            set {
                if ((this.Waktu_SampaiField.Equals(value) != true)) {
                    this.Waktu_SampaiField = value;
                    this.RaisePropertyChanged("Waktu_Sampai");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="PinguinAirService.IPinguinAirService")]
    public interface IPinguinAirService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPinguinAirService/getAllDetails", ReplyAction="http://tempuri.org/IPinguinAirService/getAllDetailsResponse")]
        Travelancar_Kel09.PinguinAirService.Penerbangan3[] getAllDetails();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPinguinAirService/getAllDetails", ReplyAction="http://tempuri.org/IPinguinAirService/getAllDetailsResponse")]
        System.Threading.Tasks.Task<Travelancar_Kel09.PinguinAirService.Penerbangan3[]> getAllDetailsAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPinguinAirService/getID", ReplyAction="http://tempuri.org/IPinguinAirService/getIDResponse")]
        Travelancar_Kel09.PinguinAirService.Penerbangan3[] getID(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPinguinAirService/getID", ReplyAction="http://tempuri.org/IPinguinAirService/getIDResponse")]
        System.Threading.Tasks.Task<Travelancar_Kel09.PinguinAirService.Penerbangan3[]> getIDAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPinguinAirService/getAsal", ReplyAction="http://tempuri.org/IPinguinAirService/getAsalResponse")]
        Travelancar_Kel09.PinguinAirService.Penerbangan3[] getAsal(string asal);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPinguinAirService/getAsal", ReplyAction="http://tempuri.org/IPinguinAirService/getAsalResponse")]
        System.Threading.Tasks.Task<Travelancar_Kel09.PinguinAirService.Penerbangan3[]> getAsalAsync(string asal);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPinguinAirServiceChannel : Travelancar_Kel09.PinguinAirService.IPinguinAirService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PinguinAirServiceClient : System.ServiceModel.ClientBase<Travelancar_Kel09.PinguinAirService.IPinguinAirService>, Travelancar_Kel09.PinguinAirService.IPinguinAirService {
        
        public PinguinAirServiceClient() {
        }
        
        public PinguinAirServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PinguinAirServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PinguinAirServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PinguinAirServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Travelancar_Kel09.PinguinAirService.Penerbangan3[] getAllDetails() {
            return base.Channel.getAllDetails();
        }
        
        public System.Threading.Tasks.Task<Travelancar_Kel09.PinguinAirService.Penerbangan3[]> getAllDetailsAsync() {
            return base.Channel.getAllDetailsAsync();
        }
        
        public Travelancar_Kel09.PinguinAirService.Penerbangan3[] getID(int id) {
            return base.Channel.getID(id);
        }
        
        public System.Threading.Tasks.Task<Travelancar_Kel09.PinguinAirService.Penerbangan3[]> getIDAsync(int id) {
            return base.Channel.getIDAsync(id);
        }
        
        public Travelancar_Kel09.PinguinAirService.Penerbangan3[] getAsal(string asal) {
            return base.Channel.getAsal(asal);
        }
        
        public System.Threading.Tasks.Task<Travelancar_Kel09.PinguinAirService.Penerbangan3[]> getAsalAsync(string asal) {
            return base.Channel.getAsalAsync(asal);
        }
    }
}
