﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.PinguinAirService;

namespace Travelancar_Kel09.Controllers
{
    public class PinguinAirController : Controller
    {  
        PinguinAirServiceClient objpinguin = new PinguinAirServiceClient();
        // GET: PinguinAir 
        public ActionResult PinguinAir()
        {
            ViewBag.lstDetails = objpinguin.getAllDetails();
            return View();
        }

        public ActionResult Search(FormCollection fc)
        {
            string txtValue = fc["SearchValue"];
            string type = fc["TypeCollection"];
            if (type.Equals("ID"))
            {
                ViewBag.lstDetails = objpinguin.getID(Convert.ToInt16(txtValue));
            }
            else
            {
                ViewBag.lstDetails = objpinguin.getAsal(Convert.ToString(txtValue));
            }
            return View("PinguinAir");
        }
    }
}