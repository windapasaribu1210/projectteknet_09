﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.Models;

namespace Travelancar_Kel09.Controllers
{
    public class PesanPesawatController : Controller
    {
        private ModelPesawat[] modelPesawats =
         {
            new ModelPesawat {Id = 1, Nama = "Rafika", Email = "rafikatampubolon18@gmail.com", No_Hp = "12345", Asal = "Sigumpar", Tujuan = "Jakarta", Tgl_Berangkat = "2012-10-20" , Kelas = "Ekonomi", Nama_Maskapai = "Puyuh Air"},
            new ModelPesawat {Id = 2, Nama = "Winda", Email = "windapasaribu21@gmail.com", No_Hp = "13456", Asal = "Sibolga", Tujuan = "Jakarta", Tgl_Berangkat = "2012-10-30" , Kelas = "Ekonomi", Nama_Maskapai = "Pinguin Air"},
            new ModelPesawat {Id = 3, Nama = "Febiola", Email = "febiola@gmail.com", No_Hp = "12568", Asal = "Balige", Tujuan = "Jakarta", Tgl_Berangkat = "2012-10-21" , Kelas = "Ekonomi", Nama_Maskapai = "Itik Air"},
            new ModelPesawat {Id = 4, Nama = "Nevi", Email = "nevidel@gmail.com", No_Hp = "19807", Asal = "Dolok Sanggul", Tujuan = "Jakarta", Tgl_Berangkat = "2012-11-20" , Kelas = "Ekonomi", Nama_Maskapai = "Puyuh Air"},
        };

        public ActionResult DetailPesanan(int? id)
        {
            ModelPesawat dataItem = modelPesawats.Where(p => p.Id == id).First();
            return View(dataItem);
        }

        public ActionResult CreatePesanan()
        {
            return View(new ModelPesawat());
        }

        [HttpPost]
        public ActionResult CreatePesanan(ModelPesawat model)
        {
            return View("DetailPesanan", model);
        }
    }
}