﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.Models;

namespace Travelancar_Kel09.Controllers
{
    public class PesanHotelController : Controller
    {
        private ModelHotel[] modelHotels =
        {
            new ModelHotel {No_Kamar = 10, Nama = "Rafika", Alamat = "Sigumpar", Email = "rafikatampubolon18@gmail.com", NoHp = "2345", CheckIn = "10-10-2000", CheckOut = "15-10-2000", Tgl_Booking = "05-10-2000"}
        };
        // GET: PesanHotel
        public ActionResult DetailHotel(int? nokmr)
        {
            ModelHotel dataHotel = modelHotels.Where(p => p.No_Kamar == nokmr).First();
            return View();
        }

        public ActionResult Booking()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Booking(ModelHotel model)
        {
            return View("DetailHotel", model);
        }
    }
}