﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.PuyuhAirService;

namespace Travelancar_Kel09.Controllers
{
    public class PuyuhAirController : Controller
    {
        PuyuhAirServiceClient objpuyuh = new PuyuhAirServiceClient();

        public ActionResult PuyuhAir()
        {
            ViewBag.lstDetails = objpuyuh.getAllDetails();
            return View();
        }

        public ActionResult Search(FormCollection fc)
        {
            string txtValue = fc["SearchValue"];
            string type = fc["TypeCollection"];
            if(type.Equals("ID"))
            {
                ViewBag.lstDetails = objpuyuh.getID(Convert.ToInt16(txtValue));
            }
            else
            {
                ViewBag.lstDetails = objpuyuh.getAsal(Convert.ToString(txtValue));
            }
            return View("PuyuhAir");
        }
    }
}