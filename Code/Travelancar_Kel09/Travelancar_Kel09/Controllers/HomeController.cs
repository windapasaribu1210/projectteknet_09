﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Travelancar_Kel09.Controllers
{
    [Authorize] 
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Pesawat()
        {
            return View();
        }

        public ActionResult Hotel()
        {
            return View();
        }

       
    }
}