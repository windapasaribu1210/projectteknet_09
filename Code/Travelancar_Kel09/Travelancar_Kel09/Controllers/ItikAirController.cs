﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.ItikAirService;

namespace Travelancar_Kel09.Controllers
{
    public class ItikAirController : Controller
    {
        ItikAirServiceClient objitik = new ItikAirServiceClient();
        // GET: ItikAir
        public ActionResult ItikAir()
        {
            ViewBag.lstDetails = objitik.getAllDetails();
            return View();
        }
        public ActionResult Search(FormCollection fc)
        {
            string txtValue = fc["SearchValue"];
            string type = fc["TypeCollection"];
            if (type.Equals("ID"))
            {
                ViewBag.lstDetails = objitik.getID(Convert.ToInt16(txtValue));
            }
            else
            {
                ViewBag.lstDetails = objitik.getAsal(Convert.ToString(txtValue));
            }
            return View("ItikAir");
        }
    }
}