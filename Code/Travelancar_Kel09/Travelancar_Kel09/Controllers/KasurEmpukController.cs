﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.KasurEmpukService;

namespace Travelancar_Kel09.Controllers
{
    public class KasurEmpukController : Controller
    {
        KasurEmpukServiceClient objempuk = new KasurEmpukServiceClient();
        // GET: KasurEmpuk
        public ActionResult KasurEmpuk()
        {
            ViewBag.lstHotel = objempuk.getAllDetails();
            return View();
        }

        public ActionResult Search(FormCollection fc)
        {
            string txtValue = fc["SearchValue"];
            string type = fc["TypeCollection"];
            if (type.Equals("ID"))
            {
                ViewBag.lstHotel = objempuk.getID(Convert.ToInt16(txtValue));
            }
            else
            {
                ViewBag.lstHotel = objempuk.getKotal(Convert.ToString(txtValue));
            }
            return View("KasurEmpuk");
        }
    }
}