﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Travelancar_Kel09.MawarMelatiService;

namespace Travelancar_Kel09.Controllers
{
    public class MawarMelatiController : Controller
    {
        MawarMelatiServiceClient objmawar = new MawarMelatiServiceClient();
        // GET: MawarMelati
        public ActionResult MawarMelati()
        {
            ViewBag.lstHotel = objmawar.getAllDetails();
            return View();
        }

        public ActionResult Search(FormCollection fc)
        {
            string txtValue = fc["SearchValue"];
            string type = fc["TypeCollection"];
            if (type.Equals("ID"))
            {
                ViewBag.lstHotel = objmawar.getID(Convert.ToInt16(txtValue));
            }
            else
            {
                ViewBag.lstHotel = objmawar.getKotal(Convert.ToString(txtValue));
            }
            return View("KasurEmpuk");
        }
    }
}