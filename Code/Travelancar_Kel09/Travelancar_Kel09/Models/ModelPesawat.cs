﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Travelancar_Kel09.Models
{
    public class ModelPesawat
    {
        
        public int Id { get; set; }
      
        public string Nama { get; set; }
  
        public string Email { get; set; }
      
        public string No_Hp { get; set; }
     
        public string Asal { get; set; }
      
        public string Tujuan { get; set; }
       
        public string Tgl_Berangkat { get; set; }
     
        public string Kelas { get; set; }
      
        public string Nama_Maskapai { get; set; }
    }
}