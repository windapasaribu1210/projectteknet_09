﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Travelancar_Kel09.Models
{
    public class ModelHotel
    {
        public int No_Kamar { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Email { get; set; }
        public string NoHp { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string Tgl_Booking { get; set; }
    }
}