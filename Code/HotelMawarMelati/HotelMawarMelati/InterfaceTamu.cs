﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace HotelMawarMelati
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface InterfaceTamu
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        string AddTamu(Tamu t);

        [OperationContract]
        DataSet GetTamuRecords();

        [OperationContract]
        string DeleteRecords(Tamu t);

        [OperationContract]
        DataSet SearchTamuRecord(Tamu t);

        [OperationContract]
        string UpdateTamuContact(Tamu t);

    }

    [DataContract]
    public class Tamu
    {
        string kode_tamu = "";
        string nama_tamu = "";
        string alamat = "";
        char no_telp;
        int no_kamar;
        DateTime check_in;
        DateTime check_out;

        [DataMember]
        public string KodeTamu
        {
            get { return kode_tamu; }
            set { kode_tamu = value; }
        }

        [DataMember]
        public string NamaTamu
        {
            get { return nama_tamu; }
            set { nama_tamu = value; }
        }

        [DataMember]
        public string Alamat
        {
            get { return alamat; }
            set { alamat = value; }
        }

        [DataMember]
        public char NoTelepon
        {
            get { return no_telp; }
            set { no_telp = value; }
        }

        [DataMember]
        public int NoKamar
        {
            get { return no_kamar; }
            set { no_kamar = value; }
        }

        [DataMember]
        public DateTime CheckIn
        {
            get { return check_in; }
            set { check_in = value; }
        }

        [DataMember]
        public DateTime CheckOut
        {
            get { return check_out; }
            set { check_out = value; }
        }

    }
}

