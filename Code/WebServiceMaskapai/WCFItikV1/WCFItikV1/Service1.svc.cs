﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace WCFItikV1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string GetData(int value)
        {
            return string.Format("Data yang dimasukkan: {0}", value);
        }


        public string Insert(InsertCust cust)
        {
            string msg;

            SqlConnection con = new SqlConnection("Data Source=DESKTOP-DLMKVA2;Initial Catalog=ItikAir_;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("Insert into Customertbl (Nama, Email , NoHp, KotaAsal, KotaTujuan, TglBerangkat, Kelas) values(@Nama, @Email, @NoHp, @KotaAsal, @KotaTujuan, @TglBerangkat, @Kelas)", con);
            cmd.Parameters.AddWithValue("@Nama", cust.nama);
            cmd.Parameters.AddWithValue("@Email ", cust.email);
            cmd.Parameters.AddWithValue("@NoHp", cust.noHp);
            cmd.Parameters.AddWithValue("@KotaAsal ", cust.kotaAsal);
            cmd.Parameters.AddWithValue("@KotaTujuan ", cust.kotaTujuan);
            cmd.Parameters.AddWithValue("@TglBerangkat ", cust.tglBerangkat);
            cmd.Parameters.AddWithValue("@Kelas", cust.kelas);

            int x = cmd.ExecuteNonQuery();
            if(x==1)
            {
                msg = "Sukses Input Data!";
            }
            else
            {
                msg = "Gaga Input Data!";
            }

            return msg;
        }

        public getdatacust GetInfo()
        {
            getdatacust g = new getdatacust();
            SqlConnection con = new SqlConnection("Data Source=DESKTOP-DLMKVA2;Initial Catalog=ItikAir_;Integrated Security=True");
            con.Open();
            SqlCommand cmd = new SqlCommand("Select * from Customertbl", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            g.custab = dt;
            return g;
        }

    }
}
