﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFItikV1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string Insert(InsertCust cust);
        [OperationContract]
        getdatacust GetInfo();
    }

    [DataContract]
    public class getdatacust
    {
        [DataMember]
        public DataTable custab
        {
            get;
            set;
        }
    }




        [DataContract]
        public class InsertCust
        {
            string Nama = string.Empty;
            string Email = string.Empty;
            string NoHp = string.Empty;
            string KotaAsal = string.Empty;
            string KotaTujuan = string.Empty;
            string TglBerangkat = string.Empty;
            string Kelas = string.Empty;


        [DataMember]
        public string nama
        {
            get { return Nama; }
            set { Nama = value; }
        }



        [DataMember]
        public string email
        {
            get { return Email; }
            set { Email = value; }
        }

        [DataMember]
        public string noHp
        {
            get { return NoHp; }
            set { NoHp = value; }
        }

        [DataMember]
        public string kotaAsal
        {
            get { return KotaAsal; }
            set { KotaAsal = value; }
        }


        [DataMember]
        public string kotaTujuan
        {
            get { return KotaTujuan; }
            set { KotaTujuan = value; }
        }


        [DataMember]
        public string tglBerangkat
        {
            get { return TglBerangkat; }
            set { TglBerangkat = value; }
        }

        [DataMember]
        public string kelas
        {
            get { return Kelas; }
            set { Kelas = value; }
        }

    }

    }

